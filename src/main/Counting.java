package main;

/*
 * Класс Counting реализует методы умножения, деления, сложения, вычитания, возведения в степень, нахождения остатка целых чисел
 */

public class Counting {
    /**
     * Метод выполняет умножение двух чисел
     *
     * @param numberOne - первый множитель
     * @param numberTwo - второй множитель
     * @return произведение
     */
    public static int multiplication(int numberOne, int numberTwo) {
        return numberOne * numberTwo;
    }
    /**
     * Метод выполняет деление двух чисел
     *
     * @param numberOne - делимое
     * @param numberTwo - делитель
     * @return частное
     */
    public static int division(int numberOne, int numberTwo) {
        return numberOne / numberTwo;
    }
    /**
     * Метод выполняет сложение двух чисел
     *
     * @param numberOne - первое слагаемое
     * @param numberTwo - второе слагаемое
     * @return сумма
     */
    public static int addition(int numberOne, int numberTwo) {
        return numberOne + numberTwo;
    }
    /**
     * Метод выполняет вычитание двух чисел
     *
     * @param numberOne - уменьшаемое
     * @param numberTwo - вычитаемое
     * @return разность
     */
    public static int subtraction(int numberOne, int numberTwo) {
        return numberOne - numberTwo;
    }
    /**
     * Метод выполняет возведение в степень двух чисел
     *
     * @param numberOne - первый множитель
     * @param numberTwo - второй множитель
     * @return степень
     */
    public static double square(int numberOne, int numberTwo) {
        return Math.pow(numberOne, numberTwo);
    }
    /**
     * Метод выполняет деление с остатком
     *
     * @param numberOne - делимое
     * @param numberTwo - делитель
     * @return остаток
     */
    public static int residue(int numberOne, int numberTwo) {
        return numberOne % numberTwo;
    }
}


