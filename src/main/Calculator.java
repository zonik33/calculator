import main.Counting;

import java.util.Scanner;

/*
 * @author Костюков К.П 17ит18
 */

public class Calculator extends Counting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            String[] arrInput = scanner.nextLine().split(" ");
            int valueOne = Integer.parseInt(arrInput[0]);
            int valueTwo = Integer.parseInt(arrInput[2]);
            switch (arrInput[1]) {
                case "*":
                    System.out.println(Counting.multiplication(valueOne, valueTwo));
                    break;
                case "/":
                    System.out.println(Counting.division(valueOne, valueTwo));
                    break;
                case "+":
                    System.out.println(Counting.addition(valueOne, valueTwo));
                    break;
                case "-":
                    System.out.println(Counting.subtraction(valueOne, valueTwo));
                    break;
                case "^":
                    System.out.println(Counting.square(valueOne, valueTwo));
                    break;
                case "%":
                    System.out.println(Counting.residue(valueOne, valueTwo));
                    break;
                default:
                    System.out.println("Неверный знак");
                    break;
            }
        } catch (NumberFormatException e) {
            System.out.println("Неверный ввод");
        } catch (ArithmeticException e) {
            System.out.println("Деление на ноль");
        } catch (Exception e) {
            System.out.println("Что-то пошло не так");
        }
    }
}